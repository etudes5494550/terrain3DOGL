# terrain3DOGL



## Description

Projet rélalisé dans le cadre de mes études, à l'aide de la librairie développée par l'université en interne : EasyCPPOGL.

Le but est de réaliser un terrain généré procéduralement, et comprenant un rendu de l'eau réaliste.

Plus d'explications sont trouvables dans le rapport.

Source des textures : https://freepbr.com/

## Resultat

![Rendu de jour](Projet_capture_jour.PNG)

![Rendu de nuit](Projet_capture_nuit.PNG)
