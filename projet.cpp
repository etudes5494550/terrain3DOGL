/////////////////////////////////		COMPILE ON WINDOWS		/////////////////////////////////

#include <iostream>
#include "shader_program.h"
#include "gl_viewer.h"
#include "ebo.h"
#include "fbo.h"
#include "texture2d.h"
#include "mesh.h"
#include "texturecubemap.h"

#define macro_str(s) #s
#define macro_xstr(s) macro_str(s)
#define DATA_PATH std::string(macro_xstr(DEF_DATA_PATH))
#define SHADERS_PATH std::string(macro_xstr(DEF_SHADERS_PATH))

using namespace EZCOGL;

//permutation table for pseudo-random
unsigned int perm[] =
{ 151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
 142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
 203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
 74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,
 105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,
 187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,
 64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
 47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,
 153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
 112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,
 235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,
 127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,
 156,180 };

float get2DPerlinNoiseValue(float x, float y, float res)
{
	float tempX, tempY;
	int x0, y0, ii, jj, gi0, gi1, gi2, gi3;
	float unit = 1.0f / sqrt(2);
	float tmp, s, t, u, v, Cx, Cy, Li1, Li2;

	//unit vectors, 8 in 2D
	float gradient2[][2] = { {unit,unit},{-unit,unit},{unit,-unit},{-unit,-unit},{1,0},{-1,0},{0,1},{0,-1} };

	//Adapting for our resolution
	x /= res;
	y /= res;

	//Getting position x and y in the grid
	x0 = (int)(x);
	y0 = (int)(y);

	//x and y masking (ii and jj values between 0 and 255 to use with permutation table)
	ii = x0 & 255;
	jj = y0 & 255;

	//retrieving values from permutation table, as indices for retrieving unit vector later
	gi0 = perm[ii + perm[jj]] % 8;
	gi1 = perm[ii + 1 + perm[jj]] % 8;
	gi2 = perm[ii + perm[jj + 1]] % 8;
	gi3 = perm[ii + 1 + perm[jj + 1]] % 8;

	//retrieving vector and weight them
	tempX = x - x0;
	tempY = y - y0;
	s = gradient2[gi0][0] * tempX + gradient2[gi0][1] * tempY;

	tempX = x - (x0 + 1);
	tempY = y - y0;
	t = gradient2[gi1][0] * tempX + gradient2[gi1][1] * tempY;

	tempX = x - x0;
	tempY = y - (y0 + 1);
	u = gradient2[gi2][0] * tempX + gradient2[gi2][1] * tempY;

	tempX = x - (x0 + 1);
	tempY = y - (y0 + 1);
	v = gradient2[gi3][0] * tempX + gradient2[gi3][1] * tempY;


	//smoothing
	tmp = x - x0;
	Cx = 3 * tmp * tmp - 2 * tmp * tmp * tmp;

	Li1 = s + Cx * (t - s);
	Li2 = u + Cx * (v - u);

	tmp = y - y0;
	Cy = 3 * tmp * tmp - 2 * tmp * tmp * tmp;

	return Li1 + Cy * (Li2 - Li1);
}

//fractal noise build height with layered perlin noise (certains canceling each others) and creating more interesting terrain 
float get2DFractalNoise(float x, float y, float terrainSizeFactor, int seed, float res, int octaves, float lacunarity, float persistence) {

	//sum of our octaves
	float height = 0;

	//coordinates that will be scaled by lacunarity
	float x0 = x+(float)seed;
	float y0 = y+(float)seed;

	//determines the effect of each octave on the sum
	float amplitude = 1;
	float amplitude2 = 0;

	for (int i = 0; i <= octaves; i++) {
		//multiply the result of perlin noise by the amplitude and add it to the sum
		height += abs(get2DPerlinNoiseValue(x0, y0, res)) * amplitude;

		//scale the perlin noise by multiplying the coordinates by lacunarity
		x0 *= lacunarity;
		y0 *= lacunarity;

		//reduce our amplitude by multiplying it by the persistence
		amplitude *= persistence;
	}
	
	// make the valleys more flat
	height *= height;

	//island-like shaped terrain
	//position from -1 to 1, based on the vertex position in the terrain grid
	float nx = (2.0f * x) / terrainSizeFactor - 1.0f;
	float ny = (2.0f * y) / terrainSizeFactor - 1.0f;
	//rescale to [0;1], 0 on the edge, 1 on the center
	float distance = 1 - (1.0f - nx * nx) * (1.0f - ny * ny);

	//new height base on an island shape
	height = (height + (1 - distance)) / 2.0f;
	return height;
}

// calculation of triangle normale
GLVec3 triangle_normal(GLVec3 v1, GLVec3 v2, GLVec3 v3) {
	GLVec3 edge1 = v2 - v1;
	GLVec3 edge2 = v3 - v1;
	GLVec3 n = (edge1.cross(edge2)).normalized();
	return n;
}

// calculate vertex normal with adjacent vertices triangles normal
std::vector<GLVec3> calculate_vertex_normal(std::vector<GLVec3> positions, int terrainSize) {

	std::vector<GLVec3> vertex_normal;
	vertex_normal.reserve(terrainSize * terrainSize);

	for (GLuint j = 0; j < terrainSize; ++j)
	{
		for (GLuint i = 0; i < terrainSize; ++i)
		{
			if (i == 0) {
				// left row
				if (j == 0) {
					// bottom left corner
					GLVec3 normal = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[i + (j + 1) * terrainSize]); //haut droite
					vertex_normal.emplace_back(normal.normalized());
				}
				else if (j == terrainSize - 1) {
					// upper left corner
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[i + (j - 1) * terrainSize], positions[(i + 1) + (j - 1) * terrainSize]); //bas
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[(i + 1) + (j - 1) * terrainSize]); //bas droite
					vertex_normal.emplace_back((normal1 + normal2).normalized());
				}
				else {
					// left row
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[i + (j + 1) * terrainSize]); //haut droite
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[i + (j-1) * terrainSize], positions[(i+1) + (j-1) * terrainSize]); //bas
					GLVec3 normal3 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[(i+1) + (j-1) * terrainSize]); //bas droite
					vertex_normal.emplace_back((normal1+normal2+normal3).normalized());
				}
			}
			else if (i == terrainSize - 1) {
				// right row
				if (j == 0) {
					// bottom right corner
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[(i - 1) + (j + 1) * terrainSize]); //haut gauche
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[i + (j + 1) * terrainSize], positions[(i - 1) + (j + 1) * terrainSize]); //haut
					vertex_normal.emplace_back((normal1 + normal2).normalized());
				}
				else if (j == terrainSize - 1) {
					// upper right corner
					GLVec3 normal = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[i + (j - 1) * terrainSize]); //bas gauche
					vertex_normal.emplace_back(normal.normalized());
				}
				else {
					// right row
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[i + (j + 1) * terrainSize], positions[(i - 1) + (j + 1) * terrainSize]); //haut
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[(i - 1) + (j + 1) * terrainSize]); //haut gauche
					GLVec3 normal3 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[i + (j - 1) * terrainSize]); //bas gauche
					vertex_normal.emplace_back((normal1 + normal2 + normal3).normalized());
				}
			}
			else {
				// all other triangles
				if (j == 0) {
					// bottom line
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[(i-1) + (j + 1) * terrainSize]); //haut gauche
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[i + (j + 1) * terrainSize], positions[(i-1) + (j+1) * terrainSize]); //haut
					GLVec3 normal3 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[i + (j + 1) * terrainSize]); //haut droite
					vertex_normal.emplace_back((normal1 + normal2 + normal3).normalized());
				}
				else if (j == terrainSize - 1) {
					// upper line
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[i + (j - 1) * terrainSize]); //bas gauche
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[i + (j - 1) * terrainSize], positions[(i + 1) + (j - 1) * terrainSize]); //bas
					GLVec3 normal3 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[(i + 1) + (j - 1) * terrainSize]); //bas droite
					vertex_normal.emplace_back((normal1 + normal2 + normal3).normalized());
				}
				else {
					// other rows and lines
					GLVec3 normal1 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[(i - 1) + (j + 1) * terrainSize]); //haut gauche
					GLVec3 normal2 = triangle_normal(positions[i + j * terrainSize], positions[i + (j + 1) * terrainSize], positions[(i - 1) + (j + 1) * terrainSize]); //haut
					GLVec3 normal3 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[i + (j + 1) * terrainSize]); //haut droite
					GLVec3 normal4 = triangle_normal(positions[i + j * terrainSize], positions[(i - 1) + j * terrainSize], positions[i + (j - 1) * terrainSize]); //bas gauche
					GLVec3 normal5 = triangle_normal(positions[i + j * terrainSize], positions[i + (j - 1) * terrainSize], positions[(i + 1) + (j - 1) * terrainSize]); //bas
					GLVec3 normal6 = triangle_normal(positions[i + j * terrainSize], positions[(i + 1) + j * terrainSize], positions[(i+1) + (j - 1) * terrainSize]); //bas droite
					vertex_normal.emplace_back((normal1 + normal2 + normal3 + normal4 + normal5 + normal6).normalized());
				}
			}
		}
	}
	return vertex_normal;
}

// initialise and load a 2D texture
void init_texture_2D(GLuint * texture, const std::string texture_path) {

	glGenTextures(1, texture);

	//Bind texture as current
	glBindTexture(GL_TEXTURE_2D, *texture);

	//Load image
	GLImage tex_image(DATA_PATH + texture_path, true, false);

	//Configure image data type for GPU and sending to it
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_image.width(), tex_image.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, tex_image.data());

	//configure filtering mode
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//wrapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//clean GL state
	glBindTexture(GL_TEXTURE_2D, 0);
}

// update light position so it behave like a sun
GLVec3 updateLightPos(float time) {
	float speed = 0.1;
	float radius = 10000;
	float t = time * speed;
	float x = radius * cos(t);
	float z = radius * sin(t);
	return GLVec3(x, 0, z);
}


class Viewer: public GLViewer
{
	//shaders
	ShaderProgram::UP shaderPrg_terrain;
	ShaderProgram::UP shaderPrg_water;
	ShaderProgram::UP shaderPrg_skybox;
	ShaderProgram::UP shaderPrg_water_refraction;
	ShaderProgram::UP shaderPrg_water_reflection;

	//fbos
	FBO_Depth::SP fbo_water_reflection;
	FBO_DepthTexture::SP fbo_water_refraction;

	//meshs
	MeshRenderer::UP skybox_renderer;

	//vao and ebo
	VAO::UP terrain_vao;
	EBO::SP terrain_ebo;
	VAO::UP water_vao;
	EBO::SP water_ebo;

	//light parameters
	GLVec3 lightPos = { 0, 0.0, 0.0 };
	GLVec3 lightColor = {1.0, 1.0, 1.0};

	//terrain parameters
	int terrainSize = 120;
	float terrainSizeFactor = 800.0;
	float terrainHeightFactor = 400.0;

	//texture
	GLuint texture_grass;
	GLuint texture_rock;
	GLuint texture_sand;
	GLuint texture_snow;
	GLuint water_distortion_map;
	GLuint water_normal_map;
	float tilingFactor = 70.0;

	TextureCubeMap::SP tex_skybox;
	TextureCubeMap::SP tex_skybox_night;

	//texture for fbos
	Texture2D::SP fbo_water_reflection_tex;
	Texture2D::SP fbo_water_refraction_tex;
	Texture2D::SP fbo_water_refraction_depth_tex;
	float fbo_tex_width = 1024;
	float fbo_tex_height = 1024;

	// water parameters
	int waterHeight = 100;
	float water_movement_factor = 0;
	float water_speed = 0.2;
	clock_t begin_time;

	//clipping planes
	GLVec4 clipplane_water_refraction = GLVec4(0, 0, -1, waterHeight);
	GLVec4 clipplane_water_reflection = GLVec4(0, 0, 1, -waterHeight);

	// skybox parameters
	float sky_movement_factor = 0;

public:
	Viewer();
	void init_ogl() override;
	void draw_ogl() override;
	void interface_ogl() override;
	void resize_ogl(int32_t w, int32_t h) override;
};

int main(int, char**)
{
	Viewer v;
	return v.launch3d();
}

Viewer::Viewer()
{}

// resize fbo
void Viewer::resize_ogl(int32_t w, int32_t h)
{
	fbo_water_reflection->resize(w, h);
	fbo_water_refraction->resize(w, h);
}

void Viewer::init_ogl()
{
	// Initialise clock

	begin_time = clock();


	// Initialise shaders programs

	shaderPrg_terrain = ShaderProgram::create({{GL_VERTEX_SHADER, load_src(SHADERS_PATH + "/projet_terrain.vs")}, {GL_FRAGMENT_SHADER, load_src(SHADERS_PATH + "/projet_terrain.fs")}}, "terrain");

	shaderPrg_water = ShaderProgram::create({ {GL_VERTEX_SHADER, load_src(SHADERS_PATH + "/projet_water.vs")}, {GL_FRAGMENT_SHADER, load_src(SHADERS_PATH + "/projet_water.fs")} }, "water");

	shaderPrg_skybox = ShaderProgram::create({ {GL_VERTEX_SHADER, load_src(SHADERS_PATH + "/projet_sky.vs")}, {GL_FRAGMENT_SHADER, load_src(SHADERS_PATH + "/projet_sky.fs")} }, "sky");

	// ***********************************
	// Geometry
	// ***********************************
	// A 2D grid for terrain
	std::vector<GLVec3> positions_terrain;
	std::vector<GLVec3> colors_terrain;
	std::vector<GLVec2> texture_coordinates_terrain;

	//seed
	srand(time(0));
	int seed = rand() * 800 +200;

	positions_terrain.reserve(terrainSize * terrainSize);
	colors_terrain.reserve(terrainSize * terrainSize);
	texture_coordinates_terrain.reserve(terrainSize * terrainSize);

	for (GLuint j = 0; j < terrainSize; ++j)
	{
		for (GLuint i = 0; i < terrainSize; ++i)
		{
			// vertex between [0;800]
			//fractal perlin noise for height, shaped to represent an island
			GLVec3 position(((float)i / (float)(terrainSize - 1)) * terrainSizeFactor/*x*/, ((float)j / (float)(terrainSize - 1)) * terrainSizeFactor/*y*/, get2DFractalNoise(((float)i / (float)(terrainSize - 1) * terrainSizeFactor), ((float)j / (float)(terrainSize - 1) * terrainSizeFactor) /*z*/, terrainSizeFactor, seed, 250.0f, 3, 3.0f, 0.35f) * terrainHeightFactor);
			positions_terrain.emplace_back(position);

			colors_terrain.emplace_back(GLVec3(1,1,1));

			// greater coordinate to repeat texture (texparameter)
			texture_coordinates_terrain.emplace_back(GLVec2((float)i / (float)(terrainSize - 1)*tilingFactor, (float)j / (float)(terrainSize - 1)*tilingFactor));
		}
	}

	// Create and fill a VBO with positions
	VBO::SP terrain_vbo_p = VBO::create(positions_terrain);
	// Create and fill a VBO with colors
	VBO::SP terrain_vbo_c = VBO::create(colors_terrain);
	// Create and fill a VBO with texture coordinates
	VBO::SP terrain_vbo_tc = VBO::create(texture_coordinates_terrain);

	//2D square for water
	std::vector<GLVec3> positions_water;

	positions_water.reserve(4);

	positions_water.emplace_back(GLVec3(-500,-500,waterHeight));
	positions_water.emplace_back(GLVec3(-500, 1300, waterHeight));
	positions_water.emplace_back(GLVec3(1300, -500, waterHeight));
	positions_water.emplace_back(GLVec3(1300, 1300, waterHeight));

	// Create and fill a VBO with positions
	VBO::SP water_vbo_p = VBO::create(positions_water);

	// Skybox
	auto meshCube = Mesh::CubePosOnly();
	skybox_renderer = meshCube->renderer(1, -1, -1, -1, -1); // We only need the 3D position of vertices


	// ***********************************
	// Topology
	// ***********************************
	// terrain
	// indices
	std::vector<GLuint> indices_terrain;
	indices_terrain.reserve((terrainSize - 1) * (terrainSize - 1) * 2 * 3);
	for (GLuint j = 0; j < terrainSize - 1; ++j)
	{
		for (GLuint i = 0; i < terrainSize - 1; ++i)
		{
			// triangle 1
			indices_terrain.emplace_back(i + j * terrainSize);
			indices_terrain.emplace_back((i + 1) + j * terrainSize);
			indices_terrain.emplace_back(i + (j + 1) * terrainSize);
			// triangle 2
			indices_terrain.emplace_back((i + 1) + j * terrainSize);
			indices_terrain.emplace_back((i + 1) + (j + 1) * terrainSize);
			indices_terrain.emplace_back(i + (j + 1) * terrainSize);
		}
	}

	// Create and fill a EBO with indices
	terrain_ebo = EBO::create(indices_terrain);

    //normal of vertices calculated with normal of adjacent vertices triangle
	std::vector<GLVec3> vertex_normal_terrain = calculate_vertex_normal(positions_terrain, terrainSize);

	VBO::SP terrain_vbo_n = VBO::create(vertex_normal_terrain);

	// Create the VAO and associate the VBOs to it
	terrain_vao = VAO::create({ {1, terrain_vbo_p}, {2, terrain_vbo_c}, {3, terrain_vbo_n}, {4, terrain_vbo_tc} });

	// water
	// indices
	std::vector<GLuint> indices_water;
	indices_water.reserve(4);

	indices_water.emplace_back(0);
	indices_water.emplace_back(1);
	indices_water.emplace_back(2);
	indices_water.emplace_back(1);
	indices_water.emplace_back(2);
	indices_water.emplace_back(3);

	// Create and fill a EBO with indices
	water_ebo = EBO::create(indices_water);

	// Create the VAO and associate the VBOs to it
	water_vao = VAO::create({ {1, water_vbo_p}});

	////////////
	//textures//
	////////////

	// terrain

	init_texture_2D(&texture_grass, "/projet/grass.png");
	init_texture_2D(&texture_rock, "/projet/rock.png");
	init_texture_2D(&texture_sand, "/projet/sand.png");
	init_texture_2D(&texture_snow, "/projet/snow.png");
	init_texture_2D(&water_distortion_map, "/projet/water_distortion_map.png");
	init_texture_2D(&water_normal_map, "/projet/water_normal_map.png");

	// skybox 
	tex_skybox = TextureCubeMap::create({ GL_LINEAR, GL_REPEAT });
	tex_skybox->load({ DATA_PATH + "/projet/skybox/skybox1/right.bmp", DATA_PATH + "/projet/skybox/skybox1/left.bmp", DATA_PATH + "/projet/skybox/skybox1/top.bmp", DATA_PATH + "/projet/skybox/skybox1/bottom.bmp", DATA_PATH + "/projet/skybox/skybox1/front.bmp", DATA_PATH + "/projet/skybox/skybox1/back.bmp" });

	tex_skybox_night = TextureCubeMap::create({ GL_LINEAR, GL_REPEAT });
	tex_skybox_night->load({ DATA_PATH + "/projet/skybox/skybox3/back.png", DATA_PATH + "/projet/skybox/skybox3/front.png", DATA_PATH + "/projet/skybox/skybox3/top.png", DATA_PATH + "/projet/skybox/skybox3/bottom.png", DATA_PATH + "/projet/skybox/skybox3/left.png", DATA_PATH + "/projet/skybox/skybox3/right.png" });

	////////
	//fbos//
	////////

	fbo_water_reflection_tex = Texture2D::create({ GL_NEAREST, GL_REPEAT });
	fbo_water_reflection_tex->init(GL_RGBA8);

	fbo_water_refraction_tex = Texture2D::create({ GL_NEAREST, GL_REPEAT });
	fbo_water_refraction_tex->init(GL_RGBA8);

	fbo_water_refraction_depth_tex = Texture2D::create({ GL_NEAREST });
	fbo_water_refraction_depth_tex->init(GL_DEPTH_COMPONENT32F);

	fbo_water_reflection = FBO_Depth::create({ fbo_water_reflection_tex });

	fbo_water_refraction = FBO_DepthTexture::create({ fbo_water_refraction_tex }, fbo_water_refraction_depth_tex);

	resize_ogl(fbo_tex_width, fbo_tex_height);

	glClearColor(0.1, 0.1, 0.1, 1.0);

	set_scene_center(GLVec3{ 400,400, 200});
	set_scene_radius(1000);
}

void Viewer::draw_ogl()
{

	////////////
	// Matrix //
	////////////

	// Get the view and projection matrix
	const GLMat4& view = this->get_view_matrix();
	const GLMat4& proj = this->get_projection_matrix();

	// matrix for reclection effect
	GLMat4 refl{
		{1.0, 0.0, 0.0, 0.0},
		{0.0, 1.0, 0.0, 0.0 },
		{0.0, 0.0, -1.0, 2.0 * 100.0},
		{0.0, 0.0, 0.0, 1.0 }
	};

	GLMat4 refl_cam = view * refl;

	// view matrix for skybox i.e without translation and scale
	GLMat4 skybox_view = view;
	skybox_view.block<3, 1>(0, 3).setZero();	// remove translation
	skybox_view.block<3, 1>(0, 0).normalize();
	skybox_view.block<3, 1>(0, 1).normalize();	// remove scale
	skybox_view.block<3, 1>(0, 2).normalize();
	GLMat4 matPV_sky = proj * skybox_view;

	sky_movement_factor += 0.02 * (float(clock() - begin_time) * 0.00001);			// angle rotation factor of skyboxes

	GLMat4 model_sky = Transfo::rotateX(90.0) * Transfo::rotateY(sky_movement_factor);					//rotation of skyboxes
	GLMat4 model_sky_refl = Transfo::rotateX(270.0) * Transfo::rotateY(-sky_movement_factor);

	// light update

	lightPos = updateLightPos((float(clock() - begin_time) * 0.001));

	// ***********************************
	// Rendering
	// ***********************************

	/////////////////////
	//render reflection//
	/////////////////////

	// save initial fbo data
	FBO::push();

	// reflection fbo

	fbo_water_reflection->bind();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CLIP_DISTANCE0);

	//skybox

	glDisable(GL_DEPTH_TEST);
	shaderPrg_skybox->bind();

	// matrix

	set_uniform_value("uMVPMatrix", matPV_sky);
	set_uniform_value("uModelMatrix", model_sky_refl);
	set_uniform_value("uSkyTex", tex_skybox->bind(0));
	set_uniform_value("uSkyNightTex", tex_skybox_night->bind(1));
	set_uniform_value("timeFactor", lightPos.z());

	skybox_renderer->draw(GL_TRIANGLES);

	// terrain

	glEnable(GL_DEPTH_TEST);

	shaderPrg_terrain->bind();
	terrain_vao->bind();
	terrain_ebo->bind();

	// matrix
	set_uniform_value(1, proj);
	set_uniform_value(2, refl_cam);
	set_uniform_value(3, Transfo::translate(0.0, 0.0, 0.0));
	set_uniform_value(4, Transfo::inverse_transpose(refl_cam));

	// light
	set_uniform_value(10, GLVec3{ -lightPos(0), lightPos(1), -lightPos(2) });		//invert light position for reflection
	set_uniform_value(11, lightColor);

	// textures
	// when multi-texturing, assign a texture unit for each sampler (activetexture)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_grass);
	set_uniform_value(20, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture_rock);
	set_uniform_value(21, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture_sand);
	set_uniform_value(22, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texture_snow);
	set_uniform_value(23, 3);

	// clipping plane
	set_uniform_value(30, clipplane_water_reflection);

	glDrawElements(GL_TRIANGLES, terrain_ebo->length(), GL_UNSIGNED_INT, NULL);


	//////////////
	//refraction//
	//////////////

	fbo_water_refraction->bind();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// skybox

	glDisable(GL_DEPTH_TEST);
	shaderPrg_skybox->bind();

	// matrix 

	set_uniform_value("uMVPMatrix", matPV_sky);
	set_uniform_value("uModelMatrix", model_sky);
	set_uniform_value("uSkyTex", tex_skybox->bind(0));
	set_uniform_value("uSkyNightTex", tex_skybox_night->bind(1));
	set_uniform_value("timeFactor", lightPos.z());

	skybox_renderer->draw(GL_TRIANGLES);

	// terrain

	glEnable(GL_DEPTH_TEST);

	shaderPrg_terrain->bind();
	terrain_vao->bind();
	terrain_ebo->bind();

	// matrix
	set_uniform_value(1, proj);
	set_uniform_value(2, view);
	set_uniform_value(3, Transfo::translate(0.0, 0.0, 0.0));
	set_uniform_value(4, Transfo::inverse_transpose(view));

	// light
	set_uniform_value(10, lightPos);
	set_uniform_value(11, lightColor);

	// textures
	// when multi-texturing, assign a texture unit for each sampler (activetexture)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_grass);
	set_uniform_value(20, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture_rock);
	set_uniform_value(21, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture_sand);
	set_uniform_value(22, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texture_snow);
	set_uniform_value(23, 3);

	// clipping plane
	set_uniform_value(30, clipplane_water_refraction);

	glDrawElements(GL_TRIANGLES, terrain_ebo->length(), GL_UNSIGNED_INT, NULL);

	// restore initial fbo data
	FBO::pop();

	///////////////////////
	//render real terrain//
	///////////////////////

	glDisable(GL_CLIP_DISTANCE0);

	// Clear the buffer before to draw the next frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// skybox

	glDisable(GL_DEPTH_TEST);
	shaderPrg_skybox->bind();

	// matrix 

	set_uniform_value("uMVPMatrix", matPV_sky);
	set_uniform_value("uModelMatrix", model_sky);
	set_uniform_value("uSkyTex", tex_skybox->bind(0));
	set_uniform_value("uSkyNightTex", tex_skybox_night->bind(1));
	set_uniform_value("timeFactor", lightPos.z());

	skybox_renderer->draw(GL_TRIANGLES);

	// terrain

	glEnable(GL_DEPTH_TEST);

	shaderPrg_terrain->bind();
	terrain_vao->bind();
	terrain_ebo->bind();

	// matrix
	set_uniform_value(1, proj);
	set_uniform_value(2, view);
	set_uniform_value(3, Transfo::translate(0.0, 0.0, 0.0));
	set_uniform_value(4, Transfo::inverse_transpose(view));

	// light
	set_uniform_value(10, lightPos);
	set_uniform_value(11, lightColor);

	// texture
	// when multi-texturing, assign a texture unit for each sampler (activetexture)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_grass);
	set_uniform_value(20, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture_rock);
	set_uniform_value(21, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture_sand);
	set_uniform_value(22, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, texture_snow);
	set_uniform_value(23, 3);

	glDrawElements(GL_TRIANGLES, terrain_ebo->length(), GL_UNSIGNED_INT, NULL);

	// water

	shaderPrg_water->bind();
	water_vao->bind();
	water_ebo->bind();

	// matrix
	set_uniform_value(1, proj);
	set_uniform_value(2, view);
	set_uniform_value(3, Transfo::translate(0.0, 0.0, 0.0));

	// light
	set_uniform_value(10, lightPos);
	set_uniform_value(11, lightColor);

	// fbo texture
	set_uniform_value(20, fbo_water_reflection_tex->bind(0));
	set_uniform_value(21, fbo_water_refraction_tex->bind(1));

	// distortion map
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, water_distortion_map);
	set_uniform_value(22, 2);

	// water movement factor
	water_movement_factor += water_speed * (float(clock() - begin_time) * 0.0000001); // water movement over time
	if (water_movement_factor > 1.0) {	// reset factor
		water_movement_factor = 0;
	}
	set_uniform_value(30, water_movement_factor);

	// fresnel effect

	//retrieve camera potition
	Eigen::Matrix4d cam_view = this->cam_.get_view_matrix_d().inverse();
	GLVec3 eye = (cam_view.block<3, 1>(0, 3)).cast<float>();
	set_uniform_value(4, eye);

	// specular effect
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, water_normal_map);
	set_uniform_value(23, 3);

	glDrawElements(GL_TRIANGLES, water_ebo->length(), GL_UNSIGNED_INT, NULL);
}

void Viewer::interface_ogl()
{}
