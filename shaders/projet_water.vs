#version 430

layout(location = 1) in vec3 position_in;
layout(location = 1) uniform mat4 mproj;
layout(location = 2) uniform mat4 mview;
layout(location = 3) uniform mat4 mmod;

layout(location = 4) uniform vec3 camera_pos;

layout(location = 10) uniform vec3 lightPos;

out vec4 clipSpace;
out vec2 texCoord;
out vec3 toCamera;
out vec3 toLight;

// MAIN PROGRAM
void main()
{

	vec4 world_position = mmod*vec4(position_in, 1.0);

	clipSpace = mproj*mview * world_position;

	float tiling = 10.0;

	texCoord = vec2((position_in.x + 500)/1800.0, (position_in.y + 500) / 1800) * tiling; //tiling for distortion map

	toCamera = camera_pos - (world_position).xyz;				// vector from water to camera

	toLight = -lightPos;					// vector from water to light

	gl_Position = clipSpace;
}
