//--------------------------------------------------------------------------------------------------------
// SKYBOX VERTEX SHADER (GLSL language)
//--------------------------------------------------------------------------------------------------------
#version 430

layout(location = 1) in vec3 position_in;

// model-view-proj matrix
uniform mat4 uMVPMatrix;
uniform mat4 uModelMatrix;

out vec3 tex_coord;

void main()
{
	// we use the position of the vertex for the 3D tex coords
	tex_coord = position_in;
	vec4 P4 = uMVPMatrix * uModelMatrix * vec4(position_in, 1.0);
	gl_Position = P4.xyww; // z coord is set to 1
}