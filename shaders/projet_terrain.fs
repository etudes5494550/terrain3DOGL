#version 430
precision highp float;

//color output
out vec4 oFragmentColor;

//INPUT from Vertex shader
in vec3 Color;
in vec3 Po;
in vec3 Po_object;
in vec3 No;
in vec3 No_object;
in vec2 texture_coord;

layout(location = 10) uniform vec3 lightPos;
layout(location = 11) uniform vec3 lightColor;
layout(location = 20) uniform sampler2D grass_sampler;
layout(location = 21) uniform sampler2D rock_sampler;
layout(location = 22) uniform sampler2D sand_sampler;
layout(location = 23) uniform sampler2D snow_sampler;

// MAIN PROGRAM
void main()
{
	//less is steeper
	float min_rock_slope = 0.5;
	float max_grass_slope = 0.9;

	//height limits
	float min_rockgrass_height = 105;
	float max_sand_height = 130;

	float min_snow_height = 220;
	float max_sandrockgrass_height = 230;

	//weights
	float rock_grass_weight = No_object.z;
	float sand_rockgrass_weight = Po_object.z;
	float snow_sandrockgrass_weight = Po_object.z;

	//min_rock_slope < rock_grass_weight < max_grass_slope
	rock_grass_weight = max(min_rock_slope, rock_grass_weight);
	rock_grass_weight = min(max_grass_slope, rock_grass_weight);

	//0 < rock_grass_weight < 1
	rock_grass_weight -= min_rock_slope;
	rock_grass_weight /= max_grass_slope-min_rock_slope;

	//min_rockgrass_height < sand_rockgrass_weight < max_sand_height
	sand_rockgrass_weight = max(min_rockgrass_height, sand_rockgrass_weight);
	sand_rockgrass_weight = min(max_sand_height, sand_rockgrass_weight);

	//0 < sand_rockgrass_weight < 1
	sand_rockgrass_weight -= min_rockgrass_height;
	sand_rockgrass_weight /= max_sand_height-min_rockgrass_height;

	//min_snow_height < snow_sandrockgrass_weight < max_sandrockgrass_height
	snow_sandrockgrass_weight = max(min_snow_height, snow_sandrockgrass_weight);
	snow_sandrockgrass_weight = min(max_sandrockgrass_height, snow_sandrockgrass_weight);

	//0 < snow_sandrockgrass_weight < 1
	snow_sandrockgrass_weight -= min_snow_height;
	snow_sandrockgrass_weight /= max_sandrockgrass_height-min_snow_height; 

	//create texture
	vec4 textureColor;
	vec4 textureGrass = texture(grass_sampler, texture_coord);
	vec4 textureRock = texture(rock_sampler, texture_coord);
	vec4 textureSand = texture(sand_sampler, texture_coord);
	vec4 textureSnow = texture(snow_sampler, texture_coord);

	//mix texture
	vec4 textureRockGrass = mix(textureRock, textureGrass, rock_grass_weight);

	vec4 textureSandRockGrass = mix(textureSand, textureRockGrass, sand_rockgrass_weight);

	textureColor = mix(textureSandRockGrass, textureSnow, snow_sandrockgrass_weight);

	//lambert shading

	vec3 N = normalize(No);
	vec3 L = normalize(lightPos);

	float d2 = dot(L, L);	// square distance from the light to the fragment

	if (gl_FrontFacing==false)
		N = -N;
	float lamb = max(0.0,dot(N,L));

	vec3 amb =  lightColor * 0.4 ;  //ambient light
	vec3 diffuse = lamb * (lightColor/d2); //fiffuse lighting from lambert BRDF

	oFragmentColor = vec4(((diffuse+amb)*textureColor.rgb)/3.14, 1.0); //divide by PI for normalization
}
