#version 430
precision highp float;

// OUTPUT
// - the default GL framebuffer is a RGBA color buffer
// - you can choose whatever name you want
// - variable is prefixed by "out"
out vec4 oFragmentColor;

// UNIFORM
// - variable is prefixed by "uniform"
// - "uniforms" are variables on GPU in "constant memory" => there values are constant during a "draw command" such as drawArrays()
// - they can be seen as user custom parameters of your shaders
// - they can be accessed in any shader (vertex, fragment)

//INPUT from Vertex shader
in vec4 clipSpace;
in vec2 texCoord;
in vec3 toCamera;
in vec3 toLight;

layout(location = 11) uniform vec3 lightColor;
layout(location = 20) uniform sampler2D reflection_sampler;
layout(location = 21) uniform sampler2D refraction_sampler;
layout(location = 22) uniform sampler2D distortion_sampler;
layout(location = 23) uniform sampler2D normalMap_sampler;

layout (location = 30) uniform float water_movement_factor;

const float distortion_strength = 0.007;

const float specularPower = 80.0;

const float specularIntensity = 0.6;

// MAIN PROGRAM
void main()
{

	vec2 ndc = (clipSpace.xy/clipSpace.w)/2.0 + 0.5;		// remap to [0,1]

	vec2 reflectionTexCoord = vec2(ndc.x, ndc.y);			//projective coordinates for texture

	vec2 refractionTexCoord = vec2(ndc.x, ndc.y);			//projective coordinates for texture

	vec2 distortionTexCoord = texture(distortion_sampler, vec2(texCoord.x + water_movement_factor, texCoord.y)).rg*0.1;		//sample distortion values

	distortionTexCoord = texCoord + vec2(distortionTexCoord.x, distortionTexCoord.y + water_movement_factor);	//distortion texture coordinates

	vec2 distortion = (texture(distortion_sampler, distortionTexCoord).rg * 2.0 -1.0) * distortion_strength;		//sample distortion map

	reflectionTexCoord += distortion;									// add distortion

	reflectionTexCoord = clamp(reflectionTexCoord, 0.001, 0.999);		// avoid wrapping texture from distortion effect

	refractionTexCoord += distortion;									// add distortion

	refractionTexCoord = clamp(refractionTexCoord, 0.001, 0.999);			// avoid wrapping texture from distortion effect

	vec4 reflectionColor = texture(reflection_sampler, reflectionTexCoord);

	vec4 refractionColor = texture(refraction_sampler, refractionTexCoord);

	vec3 viewVector = normalize(toCamera);
	float fresnelFactor = dot(vec3(0.0, 0.0, 1.0), viewVector);		// dot product between water plan vector and water to camera vector to give fresnel factor
	fresnelFactor = pow(fresnelFactor, 2.0);						// more intense reflection with fresnel

	// specular calculation with  normal map

	vec4 normalMapColor = texture(normalMap_sampler, distortionTexCoord);

	vec3 normal = normalize(vec3(normalMapColor.r * 2.0 - 1.0, normalMapColor.g*2.0 -1.0 , normalMapColor.b));

	vec3 lightReflection = reflect(normalize(toLight), normal);

	float specularFactor = max (dot(lightReflection, viewVector), 0.0);

	specularFactor = pow(specularFactor, specularPower);

	vec3 specular = lightColor * specularFactor * specularIntensity;

	//final water colour

	oFragmentColor = mix(reflectionColor, refractionColor, fresnelFactor);

	oFragmentColor = mix(oFragmentColor, vec4(0.0,0.5,0.9, 1.0), 0.1); // add blue color in water

	oFragmentColor = oFragmentColor + vec4(specular, 0.0); //add specular
}
