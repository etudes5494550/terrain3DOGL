//--------------------------------------------------------------------------------------------------------
// SKYBOX FRAGMENT SHADER (GLSL language)
//--------------------------------------------------------------------------------------------------------
#version 430

precision highp float;

in vec3 tex_coord;
uniform samplerCube uSkyTex;
uniform samplerCube uSkyNightTex;
uniform float timeFactor;

out vec4 frag;

void main()
{
	// The texture cube map is sampled with 3D coords
	vec4 day = texture(uSkyTex, tex_coord);
	vec4 night = texture(uSkyNightTex, tex_coord);
	float blendFactor = smoothstep(-0.3, 0.0, timeFactor/10000.0);		// time factor is the light height divide by the "sun" radius (max height)
	vec4 sky = mix(night, day, blendFactor);
	frag = sky;
}
