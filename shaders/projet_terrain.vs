#version 430

layout(location = 1) in vec3 position_in;
layout(location = 2) in vec3 color;
layout(location = 3) in vec3 normal_vertex;
layout(location = 4) in vec2 tex_coord;

layout(location = 1) uniform mat4 mproj;
layout(location = 2) uniform mat4 mview;
layout(location = 3) uniform mat4 mmod;
layout(location = 4) uniform mat3 normalViewMatrix;

layout(location = 30) uniform vec4 waterPlan;

out vec3 Color;
out vec3 Po;
out vec3 Po_object;
out vec3 No_object;
out vec3 No;
out vec2 texture_coord;

// MAIN PROGRAM
void main()
{
	// render only useful vertices for water refraction and reflection
	gl_ClipDistance[0] = dot(vec4(position_in,1), waterPlan);

	No_object = normal_vertex;
	Po_object = position_in;
	No = normalViewMatrix * normal_vertex;
	vec4 Po4 = mview*mmod*vec4(position_in, 1.0);

	Po = Po4.xyz;

	gl_Position = mproj*Po4;
	Color = color;
	texture_coord = tex_coord;
}
